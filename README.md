# CSS-Flex-Grid-Home-task

# How this task will be evaluated
## We expect your solution to the task to meet the following criteria:

- You have applied 2 background images in the styles
- You have commented the header section in HTML
- You have not used the style attribute in HTML
- You have written styles to one CSS file or several separate CSS files
- Your markup matches the specific image in the task
- You have uncommented the header section in HTML and arranged it horizontally
- The section with the content contains two columns
- The footer section contains 3 or 7 columns
- You have placed the information in the footer according to the image you chose: example A or example B
- Your markup matches the specific image in the task
